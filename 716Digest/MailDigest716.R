rm(list = ls())
errHandle = file('/home/admin/Logs/Logs716Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
system('rm -R "/home/admin/Dropbox/Second Gen/[SG-003S]"')
source('/home/admin/CODE/716Digest/runHistory716.R')
require('mailR')
print('History done')
source('/home/admin/CODE/716Digest/3rdGenData716.R')
print('3rd gen data called')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/716Digest/aggregateInfo.R')
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
if(F)
{
prepareSumm = function(dataread)
{
  da = nrow(dataread)
  thresh = 5
  gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
  gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
  gsismp = sum(dataread[complete.cases(dataread[,5]),5])/60000
  subdata = dataread[complete.cases(dataread[,3]),]
  subdata = subdata[as.numeric(subdata[,3]) > thresh,]
  tamb = mean(dataread[complete.cases(dataread[,10]),10])
  tambst = mean(subdata[,10])
  
  hamb = mean(dataread[complete.cases(dataread[,6]),6])
  hambst = mean(subdata[,6])
  
  tambmx = max(dataread[complete.cases(dataread[,10]),10])
  tambstmx = max(subdata[,10])
  
  hambmx = max(dataread[complete.cases(dataread[,6]),6])
  hambstmx = max(subdata[,6])
  
  tambmn = min(dataread[complete.cases(dataread[,10]),10])
  tambstmn = min(subdata[,10])
  
  hambmn = min(dataread[complete.cases(dataread[,6]),6])
  hambstmn = min(subdata[,6])
  
  tsi1 = mean(dataread[complete.cases(dataread[,3]),8])
  tsi2 = mean(dataread[complete.cases(dataread[,3]),9])
  
  gsirat = gsi1 / gsi2
  smprat = gsismp / gsi2
  
  datawrite = data.frame(Date = substr(dataread[1,1],1,10),PtsRec = da,Gsi01 = rf(gsi1), Gsi02 = rf(gsi2),Smp = rf(gsismp),
                         Tamb = rf1(tamb), TambSH = rf1(tambst),TambMx = rf1(tambmx), TambMn = rf1(tambmn),
                         TambSHmx = rf(tambstmx), TambSHmn = rf1(tambstmn), Hamb = rf1(hamb), HambSH = rf1(hambst),
                         HambMx = rf1(hambmx), HambMn = rf1(hambmn), HambSHmx = rf1(hambstmx), HambSHmn = rf1(hambstmn),
                         Tsi01 = rf1(tsi1), Tsi02= rf1(tsi2), GsiRat = rf3(gsirat), SpmRat = rf3(smprat),stringsAsFactors=FALSE)
  datawrite
}
rewriteSumm = function(datawrite)
{
  
  df = data.frame(Date = as.character(datawrite[1,1]),Gsi01 = as.character(datawrite[1,3]),Gsi02 = as.character(datawrite[1,4]),Smp = as.character(datawrite[1,5]),
                  Tamb = as.character(datawrite[1,6]),TambSH = as.character(datawrite[1,7]),Hamb = as.character(datawrite[1,12]),HambSH = as.character(datawrite[,13]),stringsAsFactors=FALSE)
  df
}
}

preparebody = function(path)
{
  print("Enter Body Func")
	body = paste(body,"Site Name: Yamazaki Mazak\n",sep="")
	body = paste(body,"\n Location: 21 Joo Koon Circle, Singapore - 629053\n")
	body = paste(body,"\n O&M Code: SG-003\n")
	body = paste(body,"\n System Size: 1621.8 kWp\n")
	body = paste(body,"\n Number of Energy Meters: 3\n")
	body = paste(body,"\n Module Brand / Model / Nos: REC / 265 / 6120\n")
	body = paste(body,"\n Inverter Brand / Model / Nos: SMA / STP60 / 21\n")
	body = paste(body,"\n Site COD: 2017-01-03\n")
	body = paste(body,"\n System age [days]:",daysactive,"\n")
	body = paste(body,"\n System age [years]:",round(daysactive/365,2),"\n")
  for(x in 1 : length(path))
  {
		print(path[x])
		dataread = read.table(path[x],header = T,sep="\t")
		if(x == 1)
			body = paste(body,"\n Stdev/COV Yields: ",format(round(as.numeric(dataread[,47])/as.numeric(dataread[,48]),2),nsmall=2),"/",as.character(dataread[,48]),"%",sep="")
		body = paste(body,"\n\n_____________________________________________\n")
		days = unlist(strsplit(path[x],"/"))
		days = substr(days[length(days)],11,20)
		body = paste(body,days," (",substr(weekdays(as.Date(days)),1,3),") FULL SITE",sep="")
		body  = paste(body,"\n_____________________________________________\n")
		body = paste(body,"\nPts Recorded: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")
		body = paste(body,"\n\nDaily Irradiation [kWh/m2]:",as.character(dataread[1,3]))
		body = paste(body,"\n\nFull site generation [kWh]:",as.character(dataread[1,29]))
		body = paste(body,"\n\nFull site Yield [kWh/kWp]:",
		as.character(round((as.numeric(dataread[1,29])/1621.8),2)))
		tempval = as.numeric(dataread[1,29])/as.numeric(dataread[1,3])
		tempval = tempval/16.218
		tempval = round(tempval,1)
		body = paste(body,"\n\nFull site PR [%]:",as.character(tempval))
		#as.character(round((as.numeric(dataread[1,29])/(1621.8*as,numeric(dataread[1,3]))),
		#1)))
		body = paste(body,"\n\nMain roof production [kWh]:",as.character(dataread[1,31]))
		body = paste(body,"\n\nCar park production [kWh]:",as.character(dataread[1,30]))
		body = paste(body,"\n\nMean Ambient Temp [C]:",as.character(dataread[1,4]))
		body = paste(body,"\n\nMean Ambient Temp Solar Hours [C]:",as.character(dataread[1,5]))
		body = paste(body,"\n\nMean Humidity [%]:",as.character(dataread[1,10]))
		body = paste(body,"\n\nMean Humidity Solar Hours [%]:",as.character(dataread[1,11]))
		body = paste(body,"\n\nMean Tmod [C]:",as.character(dataread[1,16]))
		body  = paste(body,"\n_____________________________________________\n")
		body  = paste(body,"Meter A - Rooftop")
		body  = paste(body,"\n_____________________________________________\n")
		body = paste(body,"\nEac-1 Meter-A [kWh]:",as.character(dataread[1,17]))
		body = paste(body,"\n\nEac-2 Meter-A [kWh]:",as.character(dataread[1,20]))
		body = paste(body,"\n\nYield-1 Meter-A:",as.character(dataread[1,32]))
		body = paste(body,"\n\nYield-2 Meter-A:",as.character(dataread[1,35]))
		body = paste(body,"\n\nPR-1 Meter-A [%]:",as.character(dataread[1,23]))
		body = paste(body,"\n\nPR-2 Meter-A [%]:",as.character(dataread[1,26]))
		body = paste(body,"\n\nLast recorded value Meter A [kWh]:",as.character(dataread[1,38]))
		body = paste(body,"\n\nLast recorded time Meter A:",as.character(dataread[1,49]))
		body  = paste(body,"\n\n_____________________________________________\n")
		body  = paste(body,"Meter B - Rooftop and Carpark")
		body  = paste(body,"\n_____________________________________________\n")
		body = paste(body,"\nEac-1 Meter-B [kWh]:",as.character(dataread[1,18]))
		body = paste(body,"\n\nEac-2 Meter-B [kWh]:",as.character(dataread[1,21]))
		body = paste(body,"\n\nYield-1 Meter-B:",as.character(dataread[1,33]))
		body = paste(body,"\n\nYield-2 Meter-B:",as.character(dataread[1,36]))
		body = paste(body,"\n\nPR-1 Meter-B [%]:",as.character(dataread[1,24]))
		body = paste(body,"\n\nPR-2 Meter-B [%]:",as.character(dataread[1,27]))
		body = paste(body,"\n\nLast recorded value Meter B [kWh]:",as.character(dataread[1,39]))
		body = paste(body,"\n\nLast recorded time Meter B:",as.character(dataread[1,50]))
		body  = paste(body,"\n\n_____________________________________________\n")
		body  = paste(body,"Meter C - Carpark")
		body  = paste(body,"\n_____________________________________________\n")
		body = paste(body,"\nEac-1 Meter-C [kWh]:",as.character(dataread[1,19]))
		body = paste(body,"\n\nEac-2 Meter-C [kWh]:",as.character(dataread[1,22]))
		body = paste(body,"\n\nYield-1 Meter-C:",as.character(dataread[1,34]))
		body = paste(body,"\n\nYield-2 Meter-C:",as.character(dataread[1,37]))
		body = paste(body,"\n\nPR-1 Meter-C [%]:",as.character(dataread[1,25]))
		body = paste(body,"\n\nPR-2 Meter-C [%]:",as.character(dataread[1,28]))
		body = paste(body,"\n\nLast recorded value Meter C [kWh]:",as.character(dataread[1,40]))
		body = paste(body,"\n\nLast recorded time Meter C:",as.character(dataread[1,51]))
		body  = paste(body,"\n\n_____________________________________________\n")
		body  = paste(body,"Meter B' - Rooftop B minus Carpark")
		body  = paste(body,"\n_____________________________________________\n")
		body = paste(body,"\nEac-1 Meter-C [kWh]:",as.character(dataread[1,41]))
		body = paste(body,"\n\nEac-2 Meter-C [kWh]:",as.character(dataread[1,42]))
		body = paste(body,"\n\nYield-1 Meter-C:",as.character(dataread[1,43]))
		body = paste(body,"\n\nYield-2 Meter-C:",as.character(dataread[1,44]))
		body = paste(body,"\n\nPR-1 Meter-C [%]:",as.character(dataread[1,45]))
		body = paste(body,"\n\nPR-2 Meter-C [%]:",as.character(dataread[1,46]))
  }
print('Daily Summary for Body Done')
		body = paste(body,"\n\n_____________________________________________\n\n")
  body = paste(body,"Station History")
		body = paste(body,"\n\n_____________________________________________\n\n")

	body = paste(body,"Station Birth Date:",dobstation)
	body = paste(body,"\n\n# Days station active:",daysactive)
	body = paste(body,"\n\n# Years station active:",rf1((daysactive/365)))
	#body = paste(body,"\n\nTotal irradiation from uncleaned Si sensor this month [kWh/m2]:",MONTHLYIRR1)
	#body = paste(body,"\n\nTotal irradiation from cleaned Si sensor this month [kWh/m2]:",MONTHLYIRR2)
	#body = paste(body,"\n\nTotal irradiation  measured by Pyranometer [kWh/m2]:",MONTHLYIRR3)
	# body = paste(body,"\n\nAverage irradiation from uncleaned Si sensor this month [kWh/m2]:",MONTHAVG1)
	#   body = paste(body,"\n\nAverage irradiation from cleaned Si sensor this month [kWh/m2]:",MONTHAVG2)
#		   body = paste(body,"\n\nAverage irradiation measured by Pyranometer this month [kWh/m2]:",MONTHAVG3)
#	body = paste(body,"\n\nForecasted irradiation total for uncleaned Si sensor this month [kWh/m2]:",FORECASTIRR1);
#	body = paste(body,"\n\nForecasted irradiance total for cleaned Si sensor this month [kWh/m2]:",FORECASTIRR2);
#	body = paste(body,"\n\nForecasted irradiance total for Pyranamoter this month [kWh/m2]:",FORECASTIRR3)
#	body = paste(body,"\n\nIrradiation total last 30 days from cleaned Si sensor [kWh/m2]:",LAST30DAYSTOT)
#  body = paste(body,"\n\nIrradiation total last 30 days from Pyranometer [kWh/m2]:",LAST30DAYSTOT2)
#	body = paste(body,"\n\nIrradiation average last 30 days from cleaned Si sensor [kWh/m2]:",LAST30DAYSMEAN)
#	body = paste(body,"\n\nIrradiance avg last 30 days from Pyranometer[kWh/m2]:",LAST30DAYSMEAN2)
#	body = paste(body,"\n\nSoiling loss (%):",SOILINGDEC)
#	body = paste(body,"\n\nSoiling loss per day (%)",SOILINGDECPD)
#	body = paste(body,"\n\nPyr/Si loss (%):",SNPDEC)
#	body = paste(body,"\n\nPyr/Si loss per day (%)",SNPDECPD)
print('3G Data Done')	
return(body)
}
path = "/home/admin/Dropbox/Cleantechsolar/1min/[716]"
pathwrite = "/home/admin/Dropbox/Second Gen/[SG-003S]"
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevsumname = paste("[SG-003S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")
#if(length(dir(pathmonth)) > 1)
#{
#  prevday = dir(pathmonth[length(dir(pathmonth))-1])
#}
stnnickName = "716"
stnnickName2 = "SG-003S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
	yr = substr(lastdatemail,1,4)
	monyr = substr(lastdatemail,1,7)
	prevpathyear = paste(path,yr,sep="/")
	prevsumname = paste("[",stnnickName2,"] ",substr(lastdatemail,3,4),substr(lastdatemail,6,7),".txt",sep="")
	prevpathmonth = paste(prevpathyear,monyr,sep="/")
	currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
	prevwriteyears = paste(pathwrite,yr,sep="/")
	prevwritemonths = paste(prevwriteyears,monyr,sep="/")
}

check = 0
sender <- "operations@cleantechsolar.com"
uname <- "shravan.karthik@cleantechsolar.com" 
pwd = 'CTS&*(789'
wt =1
idxdiff = 1
while(1)
{
recipients = getRecipients("SG-003S","m")
recordTimeMaster("SG-003S","Bot")
years = dir(path)
writeyears = paste(pathwrite,years[length(years)],sep="/")
checkdir(writeyears)
pathyear = paste(path,years[length(years)],sep="/")
months = dir(pathyear)
writemonths = paste(writeyears,months[length(months)],sep="/")
sumname = paste("[SG-003S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
checkdir(writemonths)
pathmonth = paste(pathyear,months[length(months)],sep="/")
tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
currday = dir(pathmonth)[length(dir(pathmonth))]
if(prevday == currday)
{
   if(wt == 1)
{
 print("")
 print("")
 print("__________________________________________________________")
 print(substr(currday,7,16))
 print("__________________________________________________________")
 print("")
 print("")
 print('Waiting')
wt = 0
}
{
  if(tm > 1319 || tm < 30)
  {
      Sys.sleep(120)
  }        
  else
  {
    Sys.sleep(3600)
  }
}
next
}
print('New Data found.. sleeping to ensure sync complete')
Sys.sleep(20)
print('Sleep Done')
print("=================================")
print(currday)
print(dir(pathmonth))
print(prevday)
print(dir(prevpathmonth))
print("=================================")
idxdiff = match(currday,dir(pathmonth)) - match(prevday,dir(prevpathmonth))
print(idxdiff)
if(idxdiff < 0)
{
	idxdiff = length(dir(prevpathmonth)) - match(prevday,dir(prevpathmonth)) + length(dir(pathmonth))
	newmonth = pathmonth
	newwritemonths = writemonths
	pathmonth = prevpathmonth
	writemonths = prevwritemonths
}
while(idxdiff)
{
{
if(prevday == dir(prevpathmonth)[length(dir(prevpathmonth))])
{
  pathmonth = newmonth
	writemonths = newwritemonths
  currday = dir(pathmonth)[1]
  monthlyirr1 = monthlyirr2 = monthlyirr3 = 0
  strng = unlist(strsplit(months[length(months)],"-"))
  daystruemonth = nodays(strng[1],strng[2])
  dayssofarmonth = 0
}	
else
currday = dir(pathmonth)[(match(prevday,dir(pathmonth)) +1)]
}
dataread = read.table(paste(pathmonth,currday,sep='/'),header = T,sep = "\t")
datawrite = prepareSumm(dataread)
datasum = rewriteSumm(datawrite)
currdayw = gsub("716","SG-003S",currday)
write.table(datawrite,file = paste(writemonths,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)



#monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3])
#monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4])
#monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5])
#globirr1 = globirr1 + as.numeric(datawrite[1,3])
#globirr2 = globirr2 + as.numeric(datawrite[1,4])
#globirr3 = globirr3 + as.numeric(datawrite[1,5])
#dayssofarmonth = dayssofarmonth + 1    
daysactive = daysactive + 1
#last30days[[last30daysidx]] = as.numeric(datawrite[1,4])
#last30days2[[last30daysidx]] = as.numeric(datawrite[1,5])



print('Digest Written');
{
  if(!file.exists(paste(writemonths,sumname,sep="/")))
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    print('Summary file created')
}
  else 
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
   print('Summary file updated')  
}
}
filetosendpath = c(paste(writemonths,currdayw,sep="/"))
filename = c(currday)
filedesc = c("Daily Digest")
dataread = read.table(paste(prevpathmonth,prevday,sep='/'),header = T,sep = "\t")
datawrite = prepareSumm(dataread)
prevdayw = gsub("716","SG-003S",prevday)
dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t")
print('Summary read')
if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
{
print('Difference in old file noted')



#previdx = (last30daysidx - 1) %% 31

#if(previdx == 0) {previdx = 1}
  datasum = rewriteSumm(datawrite)
  write.table(datawrite,file = paste(prevwritemonths,prevdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)


#if(prevpathmonth == pathmonth)
#{
#	monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
#  monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
#	monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
#}
#globirr1 = globirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
#globirr2 = globirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
#globirr3 = globirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
#last30days[[previdx]] = as.numeric(datawrite[1,4])
#last30days2[[previdx]] = as.numeric(datawrite[1,5])



datarw = read.table(paste(prevwritemonths,prevsumname,sep="/"),header = T,sep = "\t")
  rn = match(as.character(datawrite[,1]),as.character(datarw[,1]))
  print(paste('Match found at row no',rn))
  datarw[rn,] = datasum[1,]
  write.table(datarw,file = paste(prevwritemonths,prevsumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
  filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
  filename[2] = prevday
  filedesc[2] = c("Updated Digest")
print('Updated old files.. both digest and summary file')
}

#last30daysidx = (last30daysidx + 1 ) %% 31
#if(last30daysidx == 0) {last30daysidx = 1}

#LAST30DAYSTOT = rf(sum(last30days))
#LAST30DAYSMEAN = rf(mean(last30days))
#LAST30DAYSTOT2 = rf(sum(last30days2))
#LAST30DAYSMEAN2 = rf(mean(last30days2))
#DAYSACTIVE = daysactive
#MONTHLYIRR1 = monthlyirr1
#MONTHLYIRR2 = monthlyirr2
#MONTHLYIRR3 = monthlyirr3
#MONTHAVG1 = rf(monthlyirr1 / dayssofarmonth)
#MONTHAVG2 = rf(monthlyirr2 / dayssofarmonth)
#MONTHAVG3 = rf(monthlyirr3 / dayssofarmonth)
#FORECASTIRR1 = rf(as.numeric(MONTHAVG1) * daystruemonth)
#FORECASTIRR2 = rf(as.numeric(MONTHAVG2) * daystruemonth)
#FORECASTIRR3 = rf(as.numeric(MONTHAVG3) * daystruemonth)
#SOILINGDEC = rf3(((globirr1 / globirr2) - 1) * 100)
#SNPDEC = rf3(((globirr3 / globirr2) - 1) * 100)
#SOILINGDECPD = rf3(as.numeric(SOILINGDEC) / DAYSACTIVE)
#SNPDECPD = rf3(as.numeric(SNPDEC) / DAYSACTIVE)

print('Sending mail')
body = ""
body = preparebody(filetosendpath)
body = gsub("\n ","\n",body)
send.mail(from = sender,
          to = recipients,
          subject = paste("Station [SG-003S] Digest",substr(currday,7,16)),
          body = body,
          smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
          authenticate = TRUE,
          send = TRUE,
          attach.files = filetosendpath,
          file.names = filename, # optional parameter
          file.descriptions = filedesc, # optional parameter
          debug = F)
print('Mail Sent');
recordTimeMaster("SG-003S","Mail",substr(currday,7,16))
prevday = currday
prevpathyear = pathyear
prevpathmonth = pathmonth
prevwriteyears = writeyears
prevwritemonths = writemonths
prevsumname = sumname
idxdiff = idxdiff - 1;
wt = 1
}
gc()
}
sink()
