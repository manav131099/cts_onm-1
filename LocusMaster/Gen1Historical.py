import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys

accessurl ="https://api.locusenergy.com/oauth/token"
clientid = "0091591f4bf5f17693e1ee22acd2ee2c"
clientsecret = "7abff4f549770bc025df5eb371ce6857"
											
def authenticate():
    client_auth = requests.auth.HTTPBasicAuth(clientid, clientsecret)
    post_data = {"grant_type" : "password","username" : "shravan1994@gmail.com","password" : "welcome",
                 "client_id" : clientid,"client_secret" : clientsecret}
    												
    response = requests.post(accessurl,auth=client_auth,data=post_data)
    token_json = response.json()   							 
    return token_json["access_token"]  

 
	
def bot(siteid,sitename,pytimezone,timezone):
    while True:
        try:
            a=authenticate()
        except:
            print("Authentication API Failed!")
            time.sleep(5)
            continue
        break 
    def monthdelta(date, delta):
        m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
        if not m: m = 12
        d = min(date.day, [31,
            29 if y%4==0 and not y%400==0 else 28,31,30,31,30,31,31,30,31,30,31][m-1])
        return date.replace(day=d,month=m, year=y)
    strtmnth=str(monthdelta(datetime.datetime.now().date(),-1))
    tz = pytz.timezone(pytimezone)					 
    stations = [[] for _ in range(3)]
    siteId = siteid
    componenturl ="https://api.locusenergy.com/v3/sites/"+ str(siteId) +"/components"
    while True:
        try:
            r = requests.get(componenturl, headers={'Authorization': "Bearer "+a})
            data2=r.json()
            temp=data2['components']
        except:
            print("Component API Failed!")
            time.sleep(5)
            continue
        break  
    for i in data2['components']:
        if(i['nodeType']=='METER'  or i['nodeType']=='TRANSFORMER'):
            stations[0].append(i['id'])
        elif(i['nodeType']=='INVERTER'):
            stations[1].append(i['id'])
        elif(i['nodeType']=='WEATHERSTATION'):
            stations[2].append(i['id'])
    
    cols="""AphA_avg,AphA_max,AphA_min,AphB_avg,AphB_max,AphB_min,AphC_avg,AphC_max,AphC_min,Hz_avg,Hz_max,Hz_min,PF_avg,PF_max,PF_min,PI_m,PPVphAB_avg,PPVphAB_max,PPVphAB_min,PPVphBC_avg,PPVphBC_max,PPVphBC_min,PPVphCA_avg,PPVphCA_max,PPVphCA_min,
    PhVphA_avg,PhVphA_max,PhVphA_min,PhVphB_avg,PhVphB_max,PhVphB_min,PhVphC_avg,PhVphC_max,PhVphC_min,THDA%_avg,THDA%_max,THDA%_min,THDV%_avg,THDV%_max,TotVARhExp_max,THDV%_min,
    TotVARhImp_max,TotVAhExp_avg,TotVAhExp_max,TotVAhExp_min,TotVAhImp_avg,TotVAhImp_max,TotVAhImp_min,TotWhExp_max,TotWhImp_max,VA_avg,VA_max,VA_min,VAR_avg,VAR_max,VAR_min,VARphA_avg,
    VARphA_max,VARphA_min,VARphB_avg,VARphB_max,VARphB_min,VARphC_avg,VARphC_max,VARphC_min,VAphA_avg,VAphA_max,VAphA_min,VAphB_avg,VAphB_max,VAphB_min,VAphC_avg,VAphC_max,W_avg,VAphC_min
    ,W_max,W_min,W_m_avg,W_no_avg,W_no_min,W_no_max,Wh_sum,Wh_m_sum,Wh_no_sum,WphA_avg,WphA_max,WphA_min,WphB_avg,WphB_max,WphB_min,WphC_avg,WphC_max,WphC_min,DCA1_avg,
    DCA1_max,DCA1_min,DCA2_avg,DCA2_max,DCA2_min,DCV1_avg,DCV1_max,DCV1_min,DCV2_avg,DCV2_max,DCV2_min,DCW_avg,DCW_max,DCW_min,DCW01_avg,DCW01_max,DCW01_min,DCW02_avg,DCW02_max,
    DCW02_min,HoursRun_avg,HoursRun_max,HoursRun_min,InvEff_avg,InvEff_max,InvEff_min,Tmp_avg,Tmp_max,Tmp_min,Humidity_avg,Humidity_max,Humidity_min,TmpAmb_avg,TmpAmb_max,TmpAmb_min,POAI_avg,
    POAI_max,POAI_min,POAIh_sum,TmpBOM_avg,TmpBOM_max,TmpBOM_min"""

    ogcols=['ts','id','AphA_avg', 'AphA_max', 'AphA_min', 'AphB_avg', 'AphB_max', 'AphB_min', 'AphC_avg', 'AphC_max', 'AphC_min', 'DHI_m_avg', 'DHIh_m_sum', 'DNI_m_avg', 'DNIh_m_sum', 'GHI_m_avg', 'GHI_m_max', 'GHI_m_min',
    'GHIh_m_sum', 'Hz_avg', 'Hz_max', 'Hz_min', 'PF_avg', 'PF_max', 'PF_min', 'PI_m', 'PPVphAB_avg', 'PPVphAB_max', 'PPVphAB_min', 'PPVphBC_avg', 'PPVphBC_max', 'PPVphBC_min', 'PPVphCA_avg', 'PPVphCA_max', 'PPVphCA_min',
    'PhVphA_avg', 'PhVphA_max', 'PhVphA_min', 'PhVphB_avg', 'PhVphB_max', 'PhVphB_min', 'PhVphC_avg', 'PhVphC_max', 'PhVphC_min', 'THDA%_avg', 'THDA%_max', 'THDA%_min', 'THDV%_avg', 'THDV%_max', 'THDV%_min', 'TotVARhExp_max', 
    'TotVARhImp_max', 'TotVAhExp_avg', 'TotVAhExp_max', 'TotVAhExp_min', 'TotVAhImp_avg', 'TotVAhImp_max', 'TotVAhImp_min', 'TotWhExp_max', 'TotWhImp_max', 'VA_avg', 'VA_max', 'VA_min', 'VAR_avg', 'VAR_max', 'VAR_min', 'VARphA_avg',
    'VARphA_max', 'VARphA_min', 'VARphB_avg', 'VARphB_max', 'VARphB_min', 'VARphC_avg', 'VARphC_max', 'VARphC_min', 'VAphA_avg', 'VAphA_max', 'VAphA_min', 'VAphB_avg', 'VAphB_max', 'VAphB_min', 'VAphC_avg', 'VAphC_max', 'VAphC_min',
    'W_avg', 'W_max', 'W_min', 'W_m_avg', 'W_no_avg', 'W_no_min', 'W_no_max', 'Wh_sum', 'Wh_m_sum', 'Wh_no_sum', 'WphA_avg', 'WphA_max', 'WphA_min', 'WphB_avg', 'WphB_max', 'WphB_min', 'WphC_avg', 'WphC_max', 'WphC_min', 'DCA1_avg',
    'DCA1_max', 'DCA1_min', 'DCA2_avg', 'DCA2_max', 'DCA2_min', 'DCV1_avg', 'DCV1_max', 'DCV1_min', 'DCV2_avg', 'DCV2_max', 'DCV2_min', 'DCW_avg', 'DCW_max', 'DCW_min', 'DCW01_avg', 'DCW01_max', 'DCW01_min', 'DCW02_avg', 'DCW02_max', 
    'DCW02_min', 'HoursRun_avg', 'HoursRun_max', 'HoursRun_min', 'InvEff_avg', 'InvEff_max', 'InvEff_min', 'Tmp_avg', 'Tmp_max', 'Tmp_min', 'Humidity_avg', 'Humidity_max', 'Humidity_min', 'TmpAmb_avg', 'TmpAmb_max', 'TmpAmb_min', 'POAI_avg',
    'POAI_max', 'POAI_min', 'POAIh_sum', 'TmpBOM_avg', 'TmpBOM_max', 'TmpBOM_min']

    startpath="/home/admin/Start/"
    path="/home/admin/Dropbox/Gen 1 Data/"+sitename
    
    def chkdir(path):
        a=os.path.isdir(path)
        if(a):
            return
        else:
            os.makedirs(path)
            

    startdate='2018-11-16'    
    chkdir(path)
    print(("Startdate is ",startdate))
    start=startdate
    start=datetime.datetime.strptime(start, "%Y-%m-%d").date()
    final=start
    end=datetime.datetime.strptime('2019-05-30', "%Y-%m-%d").date()
    tot=end-start
    tot=tot.days
    historicalflag=0
    print("Bot Started!")
    for key,i in enumerate(stations):
        while True:
            try:
                a=authenticate()
            except:
                print("Authentication Failed!")
                time.sleep(5)
                continue
            break  
        for key2,j in enumerate(i):
            start=startdate
            start=datetime.datetime.strptime(start, "%Y-%m-%d").date()
            for k in range(0,tot):
                historicalflag=1
                end=start+datetime.timedelta(days=1)
                start2=str(start)
                temppath=path+'/'+start2[0:4]+'/'+start2[0:7]
                chkdir(temppath)
                if(key==0):
                    path2=temppath+'/MFM'+'_'+str(key2+1)
                    chkdir(path2)
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz="+timezone+"&gran=5min&fields="+cols
                elif(key==1):
                    path2=temppath+'/INVERTER'+'_'+str(key2+1)
                    chkdir(path2)
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz="+timezone+"&gran=5min&fields="+cols
                else:
                    path2=temppath+'/WMS'+'_'+str(key2+1)
                    chkdir(path2)
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz="+timezone+"&gran=5min&fields="+cols
                while True:
                    try:
                        r = requests.get(componenturl3, headers={'Authorization': "Bearer "+a})
                        data4=r.json()
                        dataframe=pd.DataFrame(data4['data'])
                    except:
                        print("Historical Data API Failed!")
                        time.sleep(5)
                        continue
                    break      
                newdf=pd.DataFrame(columns=ogcols)      
                cols1=dataframe.columns.tolist()           
                for i in cols1:
                    if i in ogcols:
                        newdf[i]=dataframe[i]
                newdf['ts']=newdf['ts'].str.replace('T',' ')
                newdf.ts=newdf.ts.str[0:19]
                if(key==0):
                    newdf.to_csv(path2+'/'+sitename+'-'+'MFM'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
                elif(key==1):
                    newdf.to_csv(path2+'/'+sitename+'-'+'I'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
                else:
                    newdf.to_csv(path2+'/'+sitename+'-'+'WMS'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
                start=start+datetime.timedelta(days=1)
                final=start
    
    if(historicalflag==0):
        print("Historical Skipped!")
    else:
        print("Historical Done!")



siteid=sys.argv[1]
sitename=sys.argv[2]
pytimezone=sys.argv[3]
timezone=sys.argv[4]
bot(siteid,sitename,pytimezone,timezone)
   