import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys

tz = pytz.timezone('Asia/Calcutta')
path='/home/admin/Dropbox/Fourth_Gen/'
path2='/home/admin/Dropbox/Lifetime/'
pathflexi='/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/'
a=['[IN-031L]','[IN-038L]','[IN-045L]', '[IN-047L]', '[IN-048L]', '[IN-049L]', '[IN-052L]', '[IN-053L]', '[IN-055L]', '[IN-056L]', '[IN-059L]', '[IN-060L]', '[IN-061L]', '[IN-062L]', '[IN-063L]', '[IN-066L]', '[IN-067L]', '[IN-068L]', '[IN-071L]',  '[IN-073L]', '[IN-074L]']
irr=['','','[IN-047L]','','','[IN-047L]','','','[IN-063L]','[IN-038L]','[IN-061L]','','','','','','[IN-037C]','','','[IN-071L]','[IN-074L]']
nometers=[2,1,1,1,3,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,2]
for index,n in enumerate(a):
    if(os.path.exists(path2+n[1:8]+"-LT.txt")):
        pass
    else:
        df=pd.read_csv(path+n+'/'+n+'-lifetime.txt',sep='\t')
        if ('WMS_1.GTI' in df.columns) or ('WMS_2.GTI' in df.columns) or ('WMS_3.GTI' in df.columns) or ('WMS_4.GTI' in df.columns) or ('WMS_5.GTI' in df.columns): 
            print(n,'Loop1')
            print(df.columns)
            if('WMS_1.GTI' in df.columns):
                cols=['Date','WMS_1.GTI','MFM_1.LastTime','MFM_1.DA']
            elif 'WMS_2.GTI' in df.columns:
                cols=['Date','WMS_2.GTI','MFM_1.LastTime','MFM_1.DA']
            elif 'WMS_3.GTI' in df.columns:
                cols=['Date','WMS_3.GTI','MFM_1.LastTime','MFM_1.DA']
            elif 'WMS_4.GTI' in df.columns:
                cols=['Date','WMS_4.GTI','MFM_1.LastTime','MFM_1.DA']
            elif 'WMS_5.GTI' in df.columns:
                cols=['Date','WMS_5.GTI','MFM_1.LastTime','MFM_1.DA']
            cols2=['Date','GHI','LastT','DA']
            for m in range(nometers[index]):
                str1='MFM_'+str(m+1)+'.LastRead'
                str2='LastR-MFM'+str(m+1)
                cols.insert(2+m,str1)
                cols2.insert(2+m,str2)
            for m in range(nometers[index]):
                str1='MFM_'+str(m+1)+'.Eac1'
                str2='Eac-MFM'+str(m+1)
                cols.insert(2+nometers[index]+m,str1)
                cols2.insert(2+nometers[index]+m,str2)
            print(cols)
            df1 = df[cols].copy()
            df1.columns=cols2
            df1.insert(2, "GHI-Flag", 0)
            for m in range(nometers[index]):
                df1.insert(2+(2*(m+1)), "LastR-MFM"+str(m+1)+"-Flag", 0)
            df1['Master-Flag']=0
            print('Done')
            df1.to_csv(path2+n[1:8]+"-LT.txt",sep='\t',mode='a',index=False,header=True)
        else:
            print(n,'Loop2')
            dfdate=df[['Date']]
            if(n=='[IN-067L]'):
                df2=pd.read_csv(pathflexi+irr[index]+'/'+irr[index]+'-lifetime.txt',sep='\t')
            else:
                df2=pd.read_csv(path+irr[index]+'/'+irr[index]+'-lifetime.txt',sep='\t')
            finaldf=pd.merge(dfdate, df2, on=['Date'],sort=False,how='left')
            if(irr[index]=='[IN-038L]'):
                finaldf=finaldf[['Date','WMS_2.GTI']]
            elif(irr[index]=='[IN-037C]'):
                finaldf=finaldf[['Date','WMS.GTI']]
            else:
                finaldf=finaldf[['Date','WMS_1.GTI']]
            if(n=='[IN-073L]'):
                cols=['Date','MFM_4.LastRead','MFM_4.Eac1','MFM_4.LastTime','MFM_4.DA']
                cols2=['Date','LastR-MFM1','Eac-MFM1','LastT','DA']
            else:
                cols=['Date','MFM_1.LastTime','MFM_1.DA']
                cols2=['Date','LastT','DA']
                for m in range(nometers[index]):
                    str1='MFM_'+str(m+1)+'.LastRead'
                    str2='LastR-MFM'+str(m+1)
                    cols.insert(1+m,str1)
                    cols2.insert(1+m,str2)
                for m in range(nometers[index]):
                    str1='MFM_'+str(m+1)+'.Eac1'
                    str2='Eac-MFM'+str(m+1)
                    cols.insert(1+nometers[index]+m,str1)
                    cols2.insert(1+nometers[index]+m,str2)
                
            print(nometers[index])
            df1 = df[cols].copy()
            df1.columns=cols2
            dfmerge=pd.merge(finaldf, df1, on=['Date'],sort=False,how='left')
            dfmerge.insert(2, "GHI-Flag", 0)
            for m in range(nometers[index]):
                dfmerge.insert(2+(2*(m+1)), "LastR-MFM"+str(m+1)+"-Flag", 0)
            dfmerge['Master-Flag']=0
            df1.to_csv(path2+n[1:8]+"-LT.txt",sep='\t',mode='a',index=False,header=True)
            


while(1):
    try:
        timenow=datetime.datetime.now(tz)+datetime.timedelta(hours=-3.5)
        timenowstr=str(timenow)
        timenowdate=str(timenow.date())
        print(timenowdate)
        for index,n in enumerate(a):
            print(n)
            df=pd.read_csv(path+n+'/'+n+'-lifetime.txt',sep='\t')
            try:
                df2=pd.read_csv(path2+n[1:8]+"-LT.txt",sep='\t')
            except:
                pass
            if ('WMS_1.GTI' in df.columns) or ('WMS_2.GTI' in df.columns) or ('WMS_3.GTI' in df.columns) or ('WMS_4.GTI' in df.columns) or ('WMS_5.GTI' in df.columns): 
                if('WMS_1.GTI' in df.columns):
                    cols=['Date','WMS_1.GTI','MFM_1.LastTime','MFM_1.DA']
                elif 'WMS_2.GTI' in df.columns:
                    cols=['Date','WMS_2.GTI','MFM_1.LastTime','MFM_1.DA']
                elif 'WMS_3.GTI' in df.columns:
                    cols=['Date','WMS_3.GTI','MFM_1.LastTime','MFM_1.DA']
                elif 'WMS_4.GTI' in df.columns:
                    cols=['Date','WMS_4.GTI','MFM_1.LastTime','MFM_1.DA']
                elif 'WMS_5.GTI' in df.columns:
                    cols=['Date','WMS_5.GTI','MFM_1.LastTime','MFM_1.DA']
                cols2=['Date','GHI','LastT','DA']
                for m in range(nometers[index]):
                    str1='MFM_'+str(m+1)+'.LastRead'
                    str2='LastR-MFM'+str(m+1)
                    cols.insert(2+m,str1)
                    cols2.insert(2+m,str2)
                for m in range(nometers[index]):
                    str1='MFM_'+str(m+1)+'.Eac1'
                    str2='Eac-MFM'+str(m+1)
                    cols.insert(2+nometers[index]+m,str1)
                    cols2.insert(2+nometers[index]+m,str2)
                df1 = df[cols].copy()
                df1.columns=cols2
                df1.insert(2, "GHI-Flag", 0)
                for m in range(nometers[index]):
                    df1.insert(2+(2*(m+1)), "LastR-MFM"+str(m+1)+"-Flag", 0)
                df1['Master-Flag']=0
                if((df2['Date']==timenowdate).any()):
                    print('PRESENT!')
                    df3=df1.loc[df1['Date']==timenowdate]
                    vals=df3.values.tolist()
                    df2.loc[df2['Date']==timenowdate,df2.columns.tolist()]=vals[0]
                    df2.to_csv(path2+n[1:8]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                else:
                    print('ABSENT')
                    df3=df1.loc[df1['Date']==timenowdate]
                    if(df3.empty):
                        print("Waiting")
                    else:
                        df3.to_csv(path2+n[1:8]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
            else:
                dfdate=df[['Date']]
                if(n=='[IN-067L]'):
                    df2=pd.read_csv(pathflexi+irr[index]+'/'+irr[index]+'-lifetime.txt',sep='\t')
                else:
                    df2=pd.read_csv(path+irr[index]+'/'+irr[index]+'-lifetime.txt',sep='\t')
                finaldf=pd.merge(dfdate, df2, on=['Date'],sort=False,how='left')
                if(irr[index]=='[IN-038L]'):
                    finaldf=finaldf[['Date','WMS_2.GTI']]
                elif(irr[index]=='[IN-037C]'):
                    finaldf=finaldf[['Date','WMS.GTI']]
                else:
                    finaldf=finaldf[['Date','WMS_1.GTI']]
                if(n=='[IN-073L]'):
                    cols=['Date','MFM_4.LastRead','MFM_4.Eac1','MFM_4.LastTime','MFM_4.DA']
                    cols2=['Date','LastR-MFM1','Eac-MFM1','LastT','DA']
                else:
                    cols=['Date','MFM_1.LastTime','MFM_1.DA']
                    cols2=['Date','LastT','DA']
                    for m in range(nometers[index]):
                        str1='MFM_'+str(m+1)+'.LastRead'
                        str2='LastR-MFM'+str(m+1)
                        cols.insert(1+m,str1)
                        cols2.insert(1+m,str2)
                    for m in range(nometers[index]):
                        str1='MFM_'+str(m+1)+'.Eac1'
                        str2='Eac-MFM'+str(m+1)
                        cols.insert(1+nometers[index]+m,str1)
                        cols2.insert(1+nometers[index]+m,str2)
                df1 = df[cols].copy()
                df1.columns=cols2
                dfmerge=pd.merge(finaldf, df1, on=['Date'],sort=False,how='left')
                dfmerge.insert(2, "GHI-Flag", 0)
                for m in range(nometers[index]):
                    dfmerge.insert(2+(2*(m+1)), "LastR-MFM"+str(m+1)+"-Flag", 0)
                dfmerge['Master-Flag']=0
                if((df2['Date']==timenowdate).any()):
                    print('PRESENT!')
                    df3=dfmerge.loc[dfmerge['Date']==timenowdate]
                    vals=df3.values.tolist()
                    dfmerge.loc[dfmerge['Date']==timenowdate,dfmerge.columns.tolist()]=vals[0]
                    dfmerge.to_csv(path2+n[1:8]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                else:
                    print('ABSENT')
                    df3=dfmerge.loc[dfmerge['Date']==timenowdate]
                    if(df3.empty):
                        print("Waiting")
                    else:
                        df3.to_csv(path2+n[1:8]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
    except Exception as e:
        print(e)
        time.sleep(300)
        continue
    time.sleep(1800)