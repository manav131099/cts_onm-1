import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import ast

tz = pytz.timezone('Asia/Calcutta')
path='/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/'
path2='/home/admin/Dropbox/Lifetime/Gen-1/'
lifetimepath='/home/admin/CODE/Lifetime/Lifetime.csv'


while(1):
    dflifetime=pd.read_csv(lifetimepath)
    a=dflifetime['Flexi_Station_Name'].tolist()
    irr=dflifetime['Flexi_Irradiation'].tolist()
    metercols=[]
    b=dflifetime['Meter_Cols_Flexi'].tolist()
    for i in b:
        temp=ast.literal_eval(i)
        metercols.append(temp)
    print(a)
    print(irr)
    print(metercols)
    for index,n in enumerate(a):
        try:
            if(os.path.exists(path2+n[1:7]+"-LT.txt")):
                pass
            else:
                print(n)
                df=pd.read_csv(path+n+'/'+n+'-lifetime.txt',sep='\t',engine='python')
                if 'WMS.GTI' in df.columns or 'WMS-GTI' in df.columns and n!='[IN-020C]':
                    if 'WMS-GTI' in df.columns:
                        cols=['Date','WMS-GTI',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                    else:
                        cols=['Date','WMS.GTI',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                    cols2=['Date','GHI','LastT','DA']
                    for m in range(len(metercols[index])):
                        str1=metercols[index][m]+'.LastRead'
                        str2='LastR-'+metercols[index][m]
                        cols.insert(2+m,str1)
                        cols2.insert(2+m,str2)
                    for m in range(len(metercols[index])):
                        str1=metercols[index][m]+'.Eac1'
                        str2='Eac-'+metercols[index][m]
                        cols.insert(2+len(metercols[index])+m,str1)
                        cols2.insert(2+len(metercols[index])+m,str2)
                    df1 = df[cols].copy()
                    df1.columns=cols2
                    df1.insert(2, "GHI-Flag", 0)
                    for m in range(len(metercols[index])):
                        df1.insert(2+(2*(m+1)), "LastR-"+metercols[index][m]+"-Flag", 0)
                    for m in range(len(metercols[index])):
                        df1.insert(2+2*len(metercols[index])+(2*(m+1)),"Eac-"+metercols[index][m]+'-Flag',0)
                    df1['Master-Flag']=0
                    df1.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=True)
                else:
                    print(n,'Loop2')
                    dfdate=df[['Date']]
                    df2=pd.read_csv(path+irr[index]+'/'+irr[index]+'-lifetime.txt',sep='\t',engine='python')
                    finaldf=pd.merge(dfdate, df2, on=['Date'],sort=False,how='left')
                    finaldf=finaldf[['Date','WMS.GTI']]
                    cols=['Date',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                    cols2=['Date','LastT','DA']
                    for m in range(len(metercols[index])):
                        str1=metercols[index][m]+'.LastRead'
                        str2='LastR-'+metercols[index][m]
                        cols.insert(1+m,str1)
                        cols2.insert(1+m,str2)
                    for m in range(len(metercols[index])):
                        str1=metercols[index][m]+'.Eac1'
                        str2='Eac-'+metercols[index][m]
                        cols.insert(1+len(metercols[index])+m,str1)
                        cols2.insert(1+len(metercols[index])+m,str2)
                    df1 = df[cols].copy()
                    df1.columns=cols2
                    dfmerge=pd.merge(finaldf, df1, on=['Date'],sort=False,how='left')
                    dfmerge.insert(2, "GHI-Flag", 0)
                    for m in range(len(metercols[index])):
                        dfmerge.insert(2+(2*(m+1)), "LastR-"+metercols[index][m]+"-Flag", 0)
                    for m in range(len(metercols[index])):
                        dfmerge.insert(2+2*len(metercols[index])+(2*(m+1)),"Eac-"+metercols[index][m]+'-Flag',0)
                    dfmerge['Master-Flag']=0
                    df1.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=True)
        except Exception as e:
            print(e)
    timenow=datetime.datetime.now(tz)+datetime.timedelta(hours=-3.5)
    timenowstr=str(timenow)
    timenowdate=str(timenow.date())
    print(timenowdate)
    for index,n in enumerate(a):
        try:
            print(n)
            if(n=='Down'):
                continue
            df=pd.read_csv(path+n+'/'+n+'-lifetime.txt',sep='\t',engine='python')
            try:
                df2=pd.read_csv(path2+n[1:7]+"-LT.txt",sep='\t',engine='python')
            except:
                pass
            if 'WMS.GTI' in df.columns or 'WMS-GTI' in df.columns and n!='[IN-020C]' : 
                if 'WMS-GTI' in df.columns:
                    cols=['Date','WMS-GTI',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                else:
                    cols=['Date','WMS.GTI',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                cols2=['Date','GHI','LastT','DA']
                for m in range(len(metercols[index])):
                    str1=metercols[index][m]+'.LastRead'
                    str2='LastR-'+metercols[index][m]
                    cols.insert(2+m,str1)
                    cols2.insert(2+m,str2)
                for m in range(len(metercols[index])):
                    str1=metercols[index][m]+'.Eac1'
                    str2='Eac-'+metercols[index][m]
                    cols.insert(2+len(metercols[index])+m,str1)
                    cols2.insert(2+len(metercols[index])+m,str2)
                df1 = df[cols].copy()
                df1.columns=cols2
                df1.insert(2, "GHI-Flag", 0)
                for m in range(len(metercols[index])):
                    df1.insert(2+(2*(m+1)), "LastR-"+metercols[index][m]+"-Flag", 0)
                for m in range(len(metercols[index])):
                    df1.insert(2+2*len(metercols[index])+(2*(m+1)),"Eac-"+metercols[index][m]+'-Flag',0)
                df1['Master-Flag']=0
                for i in range(1,15):
                    pastdate=timenow+datetime.timedelta(days=-i)
                    pastdatestr=str(pastdate)
                    pastdatestr2=pastdate.strftime("%d-%m-%Y")
                    if(((df2['Date']==pastdatestr[0:10]).any() or df2['Date']==pastdatestr2[0:10]).any()):
                        pass
                    else:
                        df3=df1.loc[(df1['Date']==pastdatestr[0:10]) | (df1['Date']==pastdatestr2[0:10])]
                        if(df3.empty):
                            pass
                        else:
                            df3.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
                if((df2['Date']==timenowdate).any()):
                    print('PRESENT!')
                    df3=df1.loc[df1['Date']==timenowdate]
                    vals=df3.values.tolist()
                    print(vals[0])
                    df2.loc[df2['Date']==timenowdate,df2.columns.tolist()]=vals[0]
                    df2.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                else:
                    print('ABSENT')
                    df3=df1.loc[df1['Date']==timenowdate]
                    if(df3.empty):
                        print("Waiting")
                    else:
                        df3.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
            else:
                dfdate=df[['Date']]
                df2=pd.read_csv(path+irr[index]+'/'+irr[index]+'-lifetime.txt',sep='\t',engine='python')
                finaldf=pd.merge(dfdate, df2, on=['Date'],sort=False,how='left')
                finaldf=finaldf[['Date','WMS.GTI']]
                cols=['Date',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                cols2=['Date','LastT','DA']
                for m in range(len(metercols[index])):
                    str1=metercols[index][m]+'.LastRead'
                    str2='LastR-'+metercols[index][m]
                    cols.insert(1+m,str1)
                    cols2.insert(1+m,str2)
                for m in range(len(metercols[index])):
                    str1=metercols[index][m]+'.Eac1'
                    str2='Eac-'+metercols[index][m]
                    cols.insert(1+len(metercols[index])+m,str1)
                    cols2.insert(1+len(metercols[index])+m,str2)
                df1 = df[cols].copy()
                df1.columns=cols2
                dfmerge=pd.merge(finaldf, df1, on=['Date'],sort=False,how='left')
                dfmerge.insert(2, "GHI-Flag", 0)
                for m in range(len(metercols[index])):
                    dfmerge.insert(2+(2*(m+1)), "LastR-"+metercols[index][m]+"-Flag", 0)
                for m in range(len(metercols[index])):
                    dfmerge.insert(2+2*len(metercols[index])+(2*(m+1)),"Eac-"+metercols[index][m]+'-Flag',0)
                dfmerge['Master-Flag']=0
                for i in range(1,15):
                    pastdate=timenow+datetime.timedelta(days=-i)
                    pastdatestr=str(pastdate)
                    pastdatestr2=pastdate.strftime("%d-%m-%Y")
                    if(((df2['Date']==pastdatestr[0:10]).any() or df2['Date']==pastdatestr2[0:10]).any()):
                        pass
                    else:
                        df3=dfmerge.loc[(dfmerge['Date']==pastdatestr[0:10]) | (dfmerge['Date']==pastdatestr2[0:10])]
                        if(df3.empty):
                            pass
                        else:
                            df3.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
                if((df2['Date']==timenowdate).any()):
                    print('PRESENT!')
                    df3=dfmerge.loc[dfmerge['Date']==timenowdate]
                    vals=df3.values.tolist()
                    dfmerge.loc[dfmerge['Date']==timenowdate,dfmerge.columns.tolist()]=vals[0]
                    dfmerge.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                else:
                    print('ABSENT')
                    df3=dfmerge.loc[dfmerge['Date']==timenowdate]
                    if(df3.empty):
                        print("Waiting")
                    else:
                        df3.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
        except Exception as e:
            print(e)
    time.sleep(1800)
  
   
