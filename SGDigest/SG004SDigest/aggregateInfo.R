source('/home/admin/CODE/common/aggregate.R')

registerMeterList("SG-004S",c("M1","M2","InSameFile"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 25 #Column no for DA
aggColTemplate[3] = 19 #column for LastRead
aggColTemplate[4] = 23 #column for LastTime
aggColTemplate[5] = 5 #column for Eac-1
aggColTemplate[6] = 7 #column for Eac-2
aggColTemplate[7] = 14 #column for Yld-1
aggColTemplate[8] = 16 #column for Yld-2
aggColTemplate[9] = 9 #column for PR-1
aggColTemplate[10] = 11 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = 4 #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("SG-004S","M1",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 25 #Column no for DA
aggColTemplate[3] = 20 #column for LastRead
aggColTemplate[4] = 24 #column for LastTime
aggColTemplate[5] = 6 #column for Eac-1
aggColTemplate[6] = 8 #column for Eac-2
aggColTemplate[7] = 15 #column for Yld-1
aggColTemplate[8] = 17 #column for Yld-2
aggColTemplate[9] = 10 #column for PR-1
aggColTemplate[10] = 12 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = 4 #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("SG-004S","M2",aggNameTemplate,aggColTemplate)

