rm(list = ls(all=T))
errHandle = file('/home/admin/Logs/LogsIN019Gen1.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('rjson')

timetomins = function(x)
{
	split = unlist(strsplit(x,":"))
	timeinmins = (as.numeric(split[1])*60) + as.numeric(split[2]) +1
	return(timeinmins)
}

url = 'http://cleantech.fleximc.com/kanchan/API/json/dash_data/'
stnno = '[IN-019C]'
path = paste('/home/admin/Dropbox/Gen 1 Data/',stnno,sep="")
normalRowLength = 54
if(!file.exists(path))
	dir.create(path)
defaultRow = unlist(rep(NA,normalRowLength))
regularcolnames = c("Inverter_1_Energy_today","Inverter_1_Energy_total","Inverter_1_AC_Power",
"Inverter_1_DC_Power",
"Inverter_2_Energy_today","Inverter_2_Energy_total","Inverter_2_AC_Power",
"Inverter_2_DC_Power",
"Inverter_3_Energy_today","Inverter_3_Energy_total","Inverter_3_AC_Power",
"Inverter_3_DC_Power",
"Inverter_4_Energy_today","Inverter_4_Energy_total","Inverter_4_AC_Power",
"Inverter_4_DC_Power",
"Inverter_5_Energy_today","Inverter_5_Energy_total","Inverter_5_AC_Power",
"Inverter_5_DC_Power",
"Inverter_6_Energy_today","Inverter_6_Energy_total","Inverter_6_AC_Power",
"Inverter_6_DC_Power",
"Inverter_7_Energy_today","Inverter_7_Energy_total","Inverter_7_AC_Power",
"Inverter_7_DC_Power",
"Inverter_8_Energy_today","Inverter_8_Energy_total","Inverter_8_AC_Power",
"Inverter_8_DC_Power",
"Inverter_9_Energy_today","Inverter_9_Energy_total","Inverter_9_AC_Power",
"Inverter_9_DC_Power",
"Inverter_10_Energy_today","Inverter_10_Energy_total","Inverter_10_AC_Power",
"Inverter_10_DC_Power",
"Inverter_11_Energy_today","Inverter_11_Energy_total","Inverter_11_AC_Power",
"Inverter_11_DC_Power",
"MFM_Export_Today","MFM_Export_Week","MFM_Export_Month",
"MFM_Export_till_date","MFM_AC_Power","Ambient_temperature","Irradiation",
"Module_temperature","Active_Alerts","Date_Time")
colNoToCheckRepeat=51

while(1)
{
	request = try(fromJSON(file=url))

	if(class(request)=='try-error')
	{
		print('Couldnt get data trying again in 60s')
		Sys.sleep(60)
		next
	}
	data = unlist(request)
	time = as.POSIXlt(Sys.time(),tz="Asia/Calcutta")
	time = as.character(time)
	time = paste(substr(time,1,16),"00",sep=":")
	yr = substr(time,1,4)
	pathyr = paste(path,yr,sep="/")
	if(!file.exists(pathyr))
		dir.create(pathyr)
	mon = substr(time,1,7)
	pathmon = paste(pathyr,mon,sep="/")
	if(!file.exists(pathmon))
		dir.create(pathmon)
	day = substr(time,1,10)
	pathday = paste(pathmon,"/",stnno,' ',day,".txt",sep="")
	content = as.character(data)
	colnames = names(data)
	print('Extracted colnames')
	if(length(content) != normalRowLength)
	{
		print('Incomplete row')
		idxmatch = match(colnames,regularcolnames)
		tempcontent = defaultRow
		tempcontent[idxmatch] = content
		content = tempcontent
		colnames = regularcolnames
	}
	tmidx = timetomins(substr(time,12,16))
	data = data.frame(rbind(content),stringsAsFactors=F)
	colnames(data) = colnames
	{
	if(!file.exists(pathday))
	{
		if(tmidx != 1)
		{
			data[tmidx,]=data
			data[1,] = defaultRow
		}
	}
	else
	{
		dataread = read.table(pathday,sep="\t",header = T,stringsAsFactors=F)
		extractedRow = as.character(dataread[nrow(dataread),colNoToCheckRepeat])
		{
		if(as.character(data[,colNoToCheckRepeat]) != extractedRow)
			dataread[tmidx,]=data
		else
			print(paste('Duplicate row for',as.character(data[,normalRowLength]),'so ignoring'))
		}
		data=dataread
	}
		write.table(data,file = pathday,row.names = F,col.names = T,sep="\t",append =F)
	}
	Sys.sleep(60)
}
