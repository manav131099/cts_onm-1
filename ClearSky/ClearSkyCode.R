rm(list = ls(all = T))

require('ggplot2')
require('gridExtra')
require('insol')

d2r <-function(x)
{
  x*pi/180
}

calZen <- function(Tm, lat, lon, tz, tilt, orientation)
{
  #tilt angle between 0 and 90
  #orientation between 0 and 360, north 0, east positive(clockwise)
  jd = JD(Tm)
  sunv = sunvector(jd, lat, lon, tz)
  azi = round(sunpos(sunv)[,1],3)#azimuth of the sun
  zen = round(sunpos(sunv)[,2],3)#zenith angle
  zen = zen
  surface.norm = normalvector(tilt, orientation)
  inc = round(as.numeric(degrees(acos(sunv%*% as.vector(surface.norm)))),3)
  dec = declination(jd)*pi/180
  re = 1.000110+0.034221*cos(dec)+0.001280*sin(dec)+0.00719*cos(2*dec)+0.000077*sin(2*dec)
  Io = round(1362*re,3)#extraterrestrial direct normal irradiance
  Ioh = round(1362*re*cos(zen*pi/180))#horizontal extraterrestrial irradiance
  Ic = round(0.8298*1362*re*(cos(d2r(zen)))^1.3585*exp(-0.00135*(90-zen)), 1)
  Ioh = ifelse(zen>=90, 0, Ioh)
  Ic = ifelse(zen>=90, 0, Ic)
  out = list(Tm,zen,azi,inc, Io, Ioh, Ic)
  names(out) = c("Time","Zenith","Azimuth","Incidence", "Io", "Ioh","Ic")
  out
}

###############################################################
# Code to obtain clear-sky values given lat-long coordinates  #
###############################################################

path = "/home/admin/Data/5 CLEAR SKY MODEL"

months = dir(path)
x=y=1

#lat = 28.0437 
#lon = 77.3432
#mainpatha = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Delhi"
#mainpath = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Delhi/Txt Values"
#mainpathg = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Delhi/Graphs"

#lat = 20.2702 
#lon = 73.8397
#mainpatha = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Nashik"
#mainpath = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Nashik/Txt Values"
#mainpathg = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Nashik/Graphs"

#lat = 11.103033 
#lon = 76.985100
#mainpatha = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Coimbatore"
#mainpath = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Coimbatore/Txt Values"
#mainpathg = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Coimbatore/Graphs"

#lat = 18.516117 
#lon = 73.705956
#mainpatha = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Pune"
#mainpath = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Pune/Txt Values"
#mainpathg = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Pune/Graphs"

#lat = 30.4651 
#lon = 76.7931
#mainpatha = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Lalru"
#mainpath = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Lalru/Txt Values"
#mainpathg = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Lalru/Graphs"

#lat = 11.4775 
#lon = 104.7853
#mainpatha = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Phnom-Penh"
#mainpath = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Phnom-Penh/Txt Values"
#mainpathg = "/media/shravan/New Volume/CTSolar/CS Data/Clear Sky Phnom-Penh/Graphs"

#lat = 12.944080556
#lon = 79.944080556
#mainpatha = "/media/shravan/New Volume1/CTSolar/CS Data/Clear Sky Chennai"
#mainpath = "/media/shravan/New Volume1/CTSolar/CS Data/Clear Sky Chennai/Txt Values"
#mainpathg = "/media/shravan/New Volume1/CTSolar/CS Data/Clear Sky Chennai/Graphs"

#lat = 1.2920 
#lon = 103.6329
#mainpatha = "/home/admin/Dropbox/CS Data/Clear Sky Singapore"
#mainpath = "/home/admin/Dropbox/CS Data/Clear Sky Singapore/Txt Values"
#mainpathg = "/home/admin/Dropbox/CS Data/Clear Sky Singapore/Graphs"

#lat = 1.435461
#lon = 103.7655
#mainpatha = "/home/admin/Dropbox/CS Data/Clear Sky SGNew"
#mainpath = "/home/admin/Dropbox/CS Data/Clear Sky SGNew/Txt Values"
#mainpathg = "/home/admin/Dropbox/CS Data/Clear Sky SGNew/Graphs"

#lat = 10.6413
#lon = 104.525
#mainpatha = "/home/admin/Dropbox/CS Data/Clear Sky CMIC"
#mainpath = "/home/admin/Dropbox/CS Data/Clear Sky CMIC/Txt Values"
#mainpathg = "/home/admin/Dropbox/CS Data/Clear Sky CMIC/Graphs"

#lat = 10.825
#lon = 106.798
#mainpatha = "/home/admin/Dropbox/CS Data/Clear Sky Vietnam"
#mainpath = "/home/admin/Dropbox/CS Data/Clear Sky Vietnam/Txt Values"
#mainpathg = "/home/admin/Dropbox/CS Data/Clear Sky Vietnam/Graphs"

#lat = 10.503535
#lon = 99.138481
#mainpatha = "/home/admin/Dropbox/CS Data/Clear Sky THSite1"
#mainpath = "/home/admin/Dropbox/CS Data/Clear Sky THSite1/Txt Values"
#mainpathg = "/home/admin/Dropbox/CS Data/Clear Sky THSite1/Graphs"

#lat = 13.825511
#lon = 100.6596
#mainpatha = "/home/admin/Dropbox/CS Data/Clear Sky THSite2"
#mainpath = "/home/admin/Dropbox/CS Data/Clear Sky THSite2/Txt Values"
#mainpathg = "/home/admin/Dropbox/CS Data/Clear Sky THSite2/Graphs"

#lat = 20.243891
#lon = 100.40991
#mainpatha = "/home/admin/Dropbox/CS Data/Clear Sky THSite3"
#mainpath = "/home/admin/Dropbox/CS Data/Clear Sky THSite3/Txt Values"
#mainpathg = "/home/admin/Dropbox/CS Data/Clear Sky THSite3/Graphs"

#lat = 5.32
#lon = 100.29
#mainpatha = "/home/admin/Dropbox/CS Data/Clear Sky Penang"
#mainpath = "/home/admin/Dropbox/CS Data/Clear Sky Penang/Txt Values"
#mainpathg = "/home/admin/Dropbox/CS Data/Clear Sky Penang/Graphs"

lat = 2.725
lon = 101.76
mainpatha = "/home/admin/Dropbox/CS Data/Clear Sky KualaLampur"
mainpath = "/home/admin/Dropbox/CS Data/Clear Sky KualaLampur/Txt Values"
mainpathg = "/home/admin/Dropbox/CS Data/Clear Sky KualaLampur/Graphs"
## Set up directory structure

if(!file.exists(mainpatha))
  dir.create(mainpatha)
if(!file.exists(mainpath))
  dir.create(mainpath)
if(!file.exists(mainpathg))
  dir.create(mainpathg)

require('ggplot2')
breaks = c(1,180,360,540,720,900,1080,1260,1440)
vals = c("00:00","03:00","06:00","09:00","12:00","15:00","18:00","21:00","23:59")
## Labels
monthsac = c("01-Jan (5 deg west)","02-Feb  (5 deg west)","03-Mar (5 deg west)","04-Apr (5 deg west)","05-May (5 deg west)","06-Jun (5 deg west)","07-Jul (5 deg west)","08-Aug (5 deg west)","09-Sep (5 deg west)","10-Oct (5 deg west)","11-Nov (5 deg west)","12-Dec (5 deg west)")
seq1 = seq(from = 1, to = 1441, by = 5)  ## Sequences for different time ranges
seq2 = seq(from = 1, to = 1441, by = 10)
seq3 = seq(from = 1, to = 1441, by = 15)
seq4 = seq(from = 1, to = 1441, by = 30)
seq5 = seq(from = 1, to = 1441, by = 60)
for(x in 1 : length(months))
{
  path2 = paste(path,"/",months[x],"/",months[x],"[AVG-1]",sep="")
  days = dir(path2)
  mainpath2 = paste(mainpath,monthsac[x],sep="/")
  mainpath2g = paste(mainpathg,"/",monthsac[x],".pdf",sep="")
  ## Files for different time ranges
  if(!file.exists(mainpath2))
    dir.create(mainpath2)
  mainpath2a = paste(mainpath2,"1-min",sep="/")
  if(!file.exists(mainpath2a))
    dir.create(mainpath2a)
  mainpath2b = paste(mainpath2,"5-mins",sep="/")
  if(!file.exists(mainpath2b))
    dir.create(mainpath2b)
  mainpath2c = paste(mainpath2,"10-mins",sep="/")
  if(!file.exists(mainpath2c))
    dir.create(mainpath2c)
  mainpath2d = paste(mainpath2,"15-mins",sep="/")
  if(!file.exists(mainpath2d))
    dir.create(mainpath2d)
  mainpath2e = paste(mainpath2,"30-mins",sep="/")
  if(!file.exists(mainpath2e))
    dir.create(mainpath2e)
  mainpath2f = paste(mainpath2,"60-mins",sep="/")
  if(!file.exists(mainpath2f))
    dir.create(mainpath2f)
  ## Plot Clear-Sky Irradiance
  pdf(mainpath2g,width = 8,height = 8)
  for(y in 1 : length(days))
  {
    mainpath3 = paste(mainpath2a,"/",substr(days[y],12,16),".txt",sep="")
    dataread = read.table(paste(path2,days[y],sep="/"),header = T,sep = "\t")
    # Convert time to std time format 
    # DONOT change the below line, let it remain  India for all Countries, only
    # update the tz parameter in calZen function to reflect the time elapsed 
    # from GMT. So for Singpore, its GMT+8, keep tz for POSIXct as India, and 
    #tz for calZen as 8. Similarly for Thailand its "India", 7.

    tm = as.POSIXct(as.character(dataread$Tm),tz = "India") 
    data = data.frame(calZen(tm,lat = lat,lon = lon,tz = 8,tilt = 0,orientation = 355)) ## CalZen function as defined previously 
    data2 = data[,c(1,7)]   
    data2[,1] = substr(data2[,1],12,19)
    sum = paste("Tot =",format(round(sum(data2[,2])/60000,1),nsmall=1))
    colnames(data2) = c("Time","CSIrr")
    write.table(data2,file = mainpath3,row.names = F,col.names = T,sep = "\t",append = F)
    tm2 = NULL
    val2 = NULL
    for(z in 2 : length(seq1))
    {
      tm2 = rbind(tm2,as.character(data2[(seq1[z]),1]))
      val2 = rbind(val2,format(round(mean(data2[seq1[z-1]:(seq1[z]-1),2]),1),nsmall=1))
    }
    tm2[length(tm2)] = as.character(data2[(seq1[length(seq1)]-1),1])
    dfrm = data.frame(Time = tm2,CSIrr = val2)
    mainpath3 = paste(mainpath2b,"/",substr(days[y],12,16),".txt",sep="")
    write.table(dfrm,file = mainpath3,row.names = F,col.names = T,sep = "\t",append = F)
    
    tm2 = NULL
    val2 = NULL
    for(z in 2 : length(seq2))
    {
      tm2 = rbind(tm2,as.character(data2[(seq2[z]),1]))
      val2 = rbind(val2,format(round(mean(data2[seq2[z-1]:(seq2[z]-1),2]),1),nsmall=1))
    }
    tm2[length(tm2)] = as.character(data2[(seq2[length(seq2)]-1),1])
    dfrm = data.frame(Time = tm2,CSIrr = val2)
    mainpath3 = paste(mainpath2c,"/",substr(days[y],12,16),".txt",sep="")
    write.table(dfrm,file = mainpath3,row.names = F,col.names = T,sep = "\t",append = F)
    
    
    tm2 = NULL
    val2 = NULL
    for(z in 2 : length(seq3))
    {
      tm2 = rbind(tm2,as.character(data2[(seq3[z]),1]))
      val2 = rbind(val2,format(round(mean(data2[seq3[z-1]:(seq3[z]-1),2]),1),nsmall=1))
    }
    tm2[length(tm2)] = as.character(data2[(seq3[length(seq3)]-1),1])
    dfrm = data.frame(Time = tm2,CSIrr = val2)
    mainpath3 = paste(mainpath2d,"/",substr(days[y],12,16),".txt",sep="")
    write.table(dfrm,file = mainpath3,row.names = F,col.names = T,sep = "\t",append = F)
    
    
    tm2 = NULL
    val2 = NULL
    for(z in 2 : length(seq4))
    {
      tm2 = rbind(tm2,as.character(data2[(seq4[z]),1]))
      val2 = rbind(val2,format(round(mean(data2[seq4[z-1]:(seq4[z]-1),2]),1),nsmall=1))
    }
    tm2[length(tm2)] = as.character(data2[(seq4[length(seq4)]-1),1])
    dfrm = data.frame(Time = tm2,CSIrr = val2)
    mainpath3 = paste(mainpath2e,"/",substr(days[y],12,16),".txt",sep="")
    write.table(dfrm,file = mainpath3,row.names = F,col.names = T,sep = "\t",append = F)
    
    
    tm2 = NULL
    val2 = NULL
    for(z in 2 : length(seq5))
    {
      tm2 = rbind(tm2,as.character(data2[(seq5[z]),1]))
      val2 = rbind(val2,format(round(mean(data2[seq5[z-1]:(seq5[z]-1),2]),1),nsmall=1))
    }
    tm2[length(tm2)] = as.character(data2[(seq5[length(seq5)]-1),1])
    dfrm = data.frame(Time = tm2,CSIrr = val2)
    mainpath3 = paste(mainpath2f,"/",substr(days[y],12,16),".txt",sep="")
    write.table(dfrm,file = mainpath3,row.names = F,col.names = T,sep = "\t",append = F)
    
    df = data.frame(x = c(1:1440),y = as.numeric(data2[,2]))
    plot = ggplot(df,aes(x = x, y = y)) + geom_point() + xlab("") + ylab(expression(paste("CS Irradiance [KW/",m^2,"]"))) + ggtitle(substr(days[y],12,16))
    plot = plot + scale_x_continuous(breaks = breaks, labels = vals) + annotate("text",label = sum, x = 1140,y = 500,size = 4)
    print(plot)
  }
  dev.off()
  print(paste(monthsac[x],"Done"))
}
