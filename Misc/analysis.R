rm(list = ls())

require('data.table')
#Function to check if directory exists, if not create one
checkdir = function(path)
{
	if(!dir.exists(path))
	{
		dir.create(path)
	}
}

#Function that computes the DA and Irradiance total per hour for a day
#Make changes in this function to compute other metrics, in-particular change
#idxProbe to what needs to be calculated and update the formula on line 55
#corresponding to the metric that needs to be calculated, for example for Tamb
#set the idxProbe to 4 and the formula will be mean instead of sum on line 55

hrAnalysis = function(data)
{
	#Irradiance index is 3
	idxProbe = 3

	#Remove all NA and NAN values
	data = data[complete.cases(as.numeric(data[,idxProbe])),]

	#Consider only values above 5
	data = data[as.numeric(data[,idxProbe])> 5,]

	#Do analysis from 7am - 7pm so the hours are 7 to 18 (18:59 hrs ~ 7pm)
	HrsValid = c(7:18)

	#Vector holding Data-availability for each hour
	DA = c()
	#Vector holding Irradiance total for each hour
	IrrTot = c()

	for(x in 1 : length(HrsValid))
	{
		# Set default values as 0
		DA[x] = 0
		IrrTot[x] = 0

		# The hour values are 12 and 13th character of the time column (column 1)
		tmHr = as.numeric(substr(as.character(data[,1]),12,13))

		#Select only rows for which the hour value is same as HrsValid[x]
		idx = which(tmHr %in% HrsValid[x])
		if(length(idx))
		{
			# Data availability for the hour is number of points for that hour / 60 * 100 provided irradiance is > 5
			DA[x] = round(length(idx) / .60,1)

			# Irradiance total is the sum of all values for the hour / 60000
			IrrTot[x] = round(sum(as.numeric(data[idx,idxProbe]))/60000,2)
		}
	}


	date = substr(as.character(data[1,1]),1,10)
	#Total irradiance for the whole day
	DayIrrTot = sum(IrrTot)

	#Total DA for the day
	DayDA = round(mean(DA),1)
	datareturn = c(date,DayIrrTot,DayDA,IrrTot,DA)
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[716]"

pathwrite = "/home/admin/Dropbox/Misc/Analysis/[716]-Analysis.txt"

years = dir(path)
row = vector('list')
#date = c()
#DATot = c()
#IrrTot = c()
#DAList = vector('list',	12)
#IrrList = vector('list',12)

idx = 1
for(x in 1 : length(years))
{
	pathyears = paste(path,years[x],sep="/")
	months = dir(pathyears)
	for(y in 1 : length(months))
	{
		pathmonths = paste(pathyears,months[y],sep="/")
		days = dir(pathmonths)
		for( z in 1 : length(days))
		{
			pathday = paste(pathmonths,days[z],sep="/")
			dataread = read.table(pathday,sep="\t",header = T)
			row[[idx]] = hrAnalysis(dataread)
#			date[idx] = datawrite[1]
#			IrrTot[idx] = datawrite[2]
#			DATot[idx] = datawrite[3]
#			for( t in 1: 12)
#			{
#				IrrList[[t]][[idx]] = datawrite[(3+t)]
#				DAList[[t]][[idx]] = datawrite[(15+t)]
#			}
			print(paste(days[z],"Done"))
			idx = idx + 1
		}
	}
}

df = data.frame(row)
df = transpose(df)
#df = data.frame(date, IrrTot, DATot, IrrList[[1]], IrrList[[2]], IrrList[[3]],
#IrrList[[4]],IrrList[[5]],IrrList[[6]],IrrList[[7]],IrrList[[8]],IrrList[[9]],IrrList[[10]],
#IrrList[[11]],IrrList[[12]],DAList[[1]],DAList[[2]],DAList[[3]],DAList[[4]],
#DAList[[5]],DAList[[6]],DAList[[7]],DAList[[8]],DAList[[9]],DAList[[10]],DAList[[11]],DAList[[12]])

colnames(df) = c("Date","DailyIrrTot","DailyDATot","IrrTot7-8am","IrrTot8am-9am",
"IrrTot9-10am","IrrTot10-11am","IrrTot11-12am","IrrTot12-1pm","IrrTot1-2pm","IrrTot2-3pm",
"IrrTot3-4pm","IrrTot4-5pm","IrrTot5-6pm","IrrTot6-7pm","DA7-8am","DA8-9am","DA9-10am",
"DA10-11am","DA11-12am","DA12-1pm","DA1-2pm","DA2-3pm","DA3-4pm","DA4-5pm","DA5-6pm","DA6-7pm")

print('Writing table')
write.table(df,file=pathwrite,row.names = F,col.names = T,append=F,sep="\t")
