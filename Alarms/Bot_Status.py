import os
import pandas as pd
import time
import pyodbc

path='/home/admin/CODE/Misc/runscripts.sh'
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'
startflag=0
while(1):
    with open(path) as file: # Use file to refer to the file object
        data = file.read().split("\n")
    file_process={}
    process_dict={}
    for index,i in enumerate(data):
        if(('if' in i) and ('$RUNALL == 1' in i)):
            file_process[data[index+2].replace('Rscript ','/usr/lib/R/bin/exec/R --slave --no-restore --file=').replace('"','').strip()]=0
    running_process=[]
    data = [(int(p), c) for p, c in [x.rstrip('\n').split(' ', 1) for x in os.popen('ps h -eo pid:1,command')]]
    for i in data:
        name=i[1].strip()
        if 'Gen1Live' in name:
            name=name.split('/')
            print(name)
            running_process.append((name[5]))
        else:
            name=name.split('/')
            running_process.append('/'.join(name[-2:]))

    cnt=0  
    for i in running_process:
        for j in file_process:
            if(i.strip() in j):
                cnt=cnt+1
                file_process[j]=1
                flag=1
                break
            flag=0
        if(flag==0):
            pass

    print(cnt)
    skip=['sendGraphs','PH_Digest.R','Usage','SG-Central','Bot_Status']
    for name in file_process:
        flag=0
        for i in skip:
            if i in name:
                flag=1
                break
        if(flag==1):
            continue
        temp=[]
        if 'python' in name:
            if 'Gen1Live' in name:
                name2=name.split('/')
                temp.append(file_process[name])
                temp.append('Gen-1')
                temp.append(name2[5].split(' ')[2][1:-1].replace('-',''))
                process_dict[(name2[5])]=temp
            else:
                name2=name.split('>')
                name2=name2[0].split('/')
                temp.append(file_process[name])#status
                file_name=name2[-1]
                folder_name=name2[-2]
                if('gen1' in file_name):
                    temp.append('Gen-1')
                    print(file_name)
                    if(len(file_name)>19):
                        temp.append(file_name[0:8].replace('-','').replace('_',''))
                    else:
                        temp.append(file_name[0:6].replace('-',''))
                    temp.append(folder_name)
                elif('raw' in file_name):
                    temp.append('Raw')
                    print(file_name)
                    if(len(file_name)>19):
                        temp.append(file_name[0:8].replace('-','').replace('_',''))
                    else:
                        temp.append(file_name[0:6].replace('-',''))
                    temp.append(folder_name)
                elif('gis' in file_name):
                    temp.append('GIS')
                    temp.append(file_name.replace('.py',''))
                elif('Lifetime' in folder_name or 'Alarms' in folder_name or 'Tesco' in folder_name or 'Azure' in folder_name or 'Invoicing' in folder_name):#Folder names are Type names
                    temp.append(folder_name)
                    temp.append(file_name.replace('.py',''))
                else:
                    if(file_name.strip()=='Live.py'):#File Names are Type Names
                        temp.append('Gen-1')
                    elif('Azure' in file_name or 'azure' in file_name):
                        temp.append('Azure')
                    else:
                        temp.append(file_name.replace('-',''))#name#table_type
                    temp.append(folder_name)#foldername#table_name
                process_dict['/'.join(name2[-2:])]=temp
        else:
            name2=name.split('/')
            file_name=name2[-1]
            folder_name=name2[-2]
            temp.append(file_process[name])
            if('GIS' in folder_name):
                temp.append('GIS')
                temp.append(file_name.replace('&',''))
            elif('Final' in folder_name or 'Prelim' in folder_name):
                temp.append(folder_name)
                temp.append(file_name.replace('&',''))
            elif('Customer' in folder_name):
                temp.append('Customer Digest')
                temp.append(file_name.replace('&','')[:-3])
            elif('Mail' in file_name.replace('&','')):
                temp.append('Digest')
                if('716' in folder_name):
                    temp.append('SG003')
                else:
                    temp.append(folder_name)
            elif('Clean' in file_name.replace('&','')):
                temp.append('Gen-1')
                temp.append(folder_name) 
            elif('Fault' in file_name.replace('&','')):
                temp.append('Alarms')
                temp.append('Bot_Down_Mail') 
            else:
                temp.append(file_name.replace('&',''))
                temp.append(folder_name)
            process_dict['/'.join(name2[-2:])]=temp

    data = {'Process_Names': [elem[2] for elem in process_dict.values()], 'Process_type': [elem[1] for elem in process_dict.values()],'Process_Status': [elem[0] for elem in process_dict.values()]}
    df=pd.DataFrame.from_dict(data)
    df.loc[df['Process_type']=='Digest','Process_Names']=df.loc[df['Process_type']=='Digest','Process_Names'].str.replace('Digest','')
    df.loc[df['Process_type']=='Gen-1','Process_Names']=df.loc[df['Process_type']=='Gen-1','Process_Names'].str.replace('Digest','')
    df.loc[df['Process_type']=='Customer Digest','Process_Names']=df.loc[df['Process_type']=='Customer Digest','Process_Names'].str.replace('MailDigest','')
    df.to_csv('/home/pranav/process.csv',index=False)
    if(startflag==0):
        connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        cursor = connStr.cursor()
        with cursor.execute("TRUNCATE TABLE dbo.Bot_Status"):
            pass
        for index,row in df.iterrows():
            try:
                with cursor.execute("INSERT INTO dbo.Bot_Status([Name],[Type],[Status]) values (?, ?, ?)", row['Process_Names'], row['Process_type'], row['Process_Status']):
                    pass
                connStr.commit()
            except Exception as e:
                print(e)
        cursor.close()
        connStr.close()
        startflag=1
    else:
        connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        cursor = connStr.cursor()
        for index,row in df.iterrows():
            try:
                with cursor.execute("UPDATE dbo.Bot_Status SET [Name]='"+row['Process_Names']+"',[Type]='"+row['Process_type']+"',[Status]="+str(row['Process_Status'])+" WHERE [Type]='"+row['Process_type']+"' AND [Name]='"+row['Process_Names']+"'"):
                    pass
                connStr.commit()
            except Exception as e:
                print(e)
        cursor.close()
        connStr.close()
    print('Sleeping')
    time.sleep(300)
