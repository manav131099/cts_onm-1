import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import numpy as np
import pyodbc

path_write='/home/pranav/Alerts.txt'
tz = pytz.timezone('Asia/Calcutta')

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

def get_data(stn,date,capacities,Type):
    path='/home/admin/Dropbox/Fourth_Gen/['+stn+'L]/['+stn+'L]-lifetime.txt'
    df=pd.read_csv(path,sep='\t')
    cols=df.columns.tolist()
    temp=[]
    if(Type=='inv'):
        for i in cols:
            if(i[-4:]=='Yld2' and i[0:3]!='MFM'):
                temp.append(i)
    elif(Type=='mfm'):
        for i in cols:
            if(i[-4:]=='Yld2' and i[0:3]=='MFM'):
                temp.append(i)
    elif(Type=='pr'):
        for i in cols:
            if(i[-3:]=='PR2' and i[0:3]=='MFM'):
                temp.append(i)
    elif(Type=='meterdata'):
        for i in cols:
            if(i[-4:]=='Eac2' and i[0:3]=='MFM'):
                temp.append(i)
    df_Yld=df[['Date']+temp].fillna(np.nan).copy()
    df_Subset=df_Yld.loc[df_Yld['Date']==date,].dropna(axis=1)
    vals=[]
    if(df_Subset.empty):
        pass
    else:
        vals=df_Subset.values[0][1:]
    if(len(vals)==0):
        return [None,None]
    else:
        if(Type=='inv' or Type=='mfm'):
            print(vals)
            if(np.mean(vals)==0):
                return 0.0
            cov=round(np.std(vals)*100/np.mean(vals),1)
            return cov
        elif(Type=='pr'):
            temp2=0
            for i in range(len(capacities)):
                temp2=temp2+capacities[i]*vals[i]
            return temp2/sum(capacities)
        elif(Type=='meterdata'):
            temp2=0
            yields=[]
            for i in range(len(capacities)):
                yields.append(vals[i]/capacities[i])
            for i in range(len(capacities)):
                temp2=temp2+capacities[i]*yields[i]
            return [sum(vals),temp2/sum(capacities)]

def get_pa_ga(stn,date,capacities):
    path='/home/admin/Dropbox/Fourth_Gen/['+stn+'L]/['+stn+'L]-lifetime.txt'
    df=pd.read_csv(path,sep='\t')
    cols=df.columns.tolist()
    ga=[]
    pa=[]
    for i in cols:
        if(i[-2:]=='GA' and i[0:3]=='MFM'):
            ga.append(i)
        elif(i[-2:]=='PA' and i[0:3]=='MFM'):
            pa.append(i)
    df_ga=df[['Date']+ga].copy()
    df_pa=df[['Date']+pa].copy()
    df_Subset_1=df_ga.loc[df_ga['Date']==date,].dropna(axis=1)
    df_Subset_2=df_pa.loc[df_pa['Date']==date,].dropna(axis=1)
    pa_vals=[]
    ga_vals=[]
    if(df_Subset_1.empty):
        pass
    else:
        ga_vals=df_Subset_1.values[0][1:]
        pa_vals=df_Subset_2.values[0][1:]
    print(ga_vals)
    if(len(ga_vals)==0 or len(pa_vals)==0):
        return [None,None]
    else:
        ga_final=0
        pa_final=0
        for i in range(len(capacities)):
            ga_final=ga_final+capacities[i]*ga_vals[i]
            pa_final=pa_final+capacities[i]*pa_vals[i]
        return [ga_final/sum(capacities),pa_final/sum(capacities)]


    
stns=['IN-021','IN-031','IN-038','IN-045','IN-047','IN-049','IN-052','IN-056','IN-060','IN-061','IN-063','IN-068','IN-074','IN-055','IN-059','IN-062','IN-066','IN-067','MY-401','IN-041','IN-018','IN-051','MY-402','MY-403','MY-404','MY-405','MY-406','IN-080','IN-081','IN-082','TH-010','TH-011','TH-012','TH-013','TH-014','TH-015','TH-016','TH-017','TH-018','TH-019','TH-020','TH-021','TH-022','TH-023','TH-024','TH-025','TH-026','TH-027','TH-028','TH-004','TH-005','TH-007','MY-007','MY-009']
stnname=['Apollo Tyres - Vadodara','Texmo Pipes and Products Ltd.','FDC Aurangabad','Fortuna Engineering HQ','Fortuna Engineering D-16',
'Fortuna Engineering S-28','Kellogg Taloja','Cosmo Films - Waluj','Exide - Ahmednagar','Amber - Ranjangaon','Musashi Bawal','EPCOS - Nasik',
'Toyota Techno Park Badadi','Sunbeam - Tapukara','Exide - Chinchwad','Exide - Hosur','Amber - Jhajjar','Amber Koregaon',
'Shell Elite Putra Heights - 12163613','Pennar Industries - Telangana','S&P Threads-Madura Coats','Hindware (HSIL) - Isnapur','Shell Putrajaya Precinct 18 - 12444271','Shell Cyberjaya - 12231175','Shell Mint Hotel - 10311889','Shell Alor Pongsu Layby NB','Shell Hentian Tangkak - 12250921','HUL Nasik','Sandvik Chiplun','Amber Jhajjar (Site 2)','TH-010','TH-011','TH-012','TH-013','TH-014','TH-015','TH-016','TH-017','TH-018','TH-019','TH-020','TH-021','TH-022','TH-023','TH-024','TH-025','TH-026','TH-027','TH-028','TH-004','TH-005','TH-007','MY-007','MY-009']
capacities=[[910],[318,445],[547.20],[133.92],[180.01],[213.84],[505.6,241.92],[503.25,446.16],[390,650],[337.03],[756.36],[995.15],[65,65],[377.0],[476.52],[442.2,501.6,264,442.2],[834.6],[368],[70.875],[760.5,130],[100.8],[325,302.58,370.5],[70.875],[58.32],[70.875],[64.8],[70.875],[1214.4],[409.2],[811.53],[994.5],[546],[994.5],[344.5],[994.5],[994.5],[702],[923],[994.5],[916.5],[994.5],[923],[994.5],[624],[650,214.5],[546],[793],[624],[546],[310.2],[812.5],[993.6],[345.8],[913.56]]
mfmcov_limit=[0,4,0,0,0,0,6,2,1,0,0,0,1,0,0,5,0,0,0,5,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0]
invcov_limit=[15,5,10,5,2,3,10,2,5,6.5,4,12,1,5,10,8,10,6,0,7,10,3,0,0,0,0,0,2,10,3,8.5,3,3,2,4,5,3,2,4,5,4,2,4,1,5,10,2,3,7,3.5,6,4,2,2.5]
while(1):
    if(datetime.datetime.now(tz).hour==1 or datetime.datetime.now(tz).hour==10):
        date=str(datetime.datetime.now(tz).date()+datetime.timedelta(days=-1))
        for index,i in enumerate(stns):
            try:
                df_vals=[]
                pr_flag=0
                mfmcov_flag=0
                invcov_flag=0
                print(i)
                mfm_cov=get_data(i,date,capacities[index],'mfm')
                inv_cov=get_data(i,date,capacities[index],'inv')
                pr=get_data(i,date,capacities[index],'pr')
                mfm_data=get_data(i,date,capacities[index],'meterdata')
                if(i[0:4]!='MY-4' and i[0:2]!='TH'):
                    data2=get_pa_ga(i,date,capacities[index])
                    if(data2[0]!=None):
                        ga=round(data2[0],2)
                        pa=round(data2[1],2)
                else:
                    data2=[None,None]
                    ga=data2[0]
                    pa=data2[1]
                eac=mfm_data[0]
                yld=mfm_data[1]
                #PR Flag
                if(i=='IN-060' or i=='IN-018' or i=='IN-062'):
                    if(pr<70):
                        pr_flag=1
                    elif(pr>90):
                        pr_flag=2
                elif(i=='IN-074'):
                    if(pr<80):
                        pr_flag=1
                    elif(pr>90):
                        pr_flag=2
                else:
                    if(pr<75):
                        pr_flag=1
                    elif(pr>90):
                        pr_flag=2
                if(inv_cov>invcov_limit[index]):
                    invcov_flag=1
                if(mfm_cov>mfmcov_limit[index]):
                    mfmcov_flag=1
                overall_flag=invcov_flag+mfmcov_flag+pr_flag
                if(pr_flag==0 and overall_flag>0):
                    overall_flag=3
                elif(overall_flag>0):
                    overall_flag=1
                else:
                    overall_flag=0
                df_vals.extend([date]+[stns[index]]+[stnname[index]]+[eac]+[yld]+[pr]+[ga]+[pa]+[mfm_cov]+[inv_cov]+[pr_flag]+[mfmcov_flag]+[invcov_flag]+[overall_flag])
                df = pd.DataFrame([df_vals], columns =['Date','O&M_Code','Site_Name','Energy','Yield','PR','GA','PA','CoV_MFM','CoV_INV','PR_Alarm','MFM_CoV_Alarm','INV_CoV_Alarm','Status']) 
                if(os.path.exists(path_write)):
                    df.to_csv(path_write,mode='a',header=False)
                else:
                    df.to_csv(path_write,mode='a',header=True) 
                connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
                cursor = connStr.cursor()
                for index,row in df.iterrows():
                    try:
                        with cursor.execute("INSERT INTO dbo.Station_Alerts([Date],[O&M_Code],[Site_Name],[Energy],[Yield],[PR],[GA],[PA],[CoV_MFM],[CoV_INV],[PR_Alarm],[MFM_CoV_Alarm],[INV_CoV_Alarm],[Status]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['O&M_Code'], row['Site_Name'], round(float(row['Energy']),2), round(float(row['Yield']),2),round(float(row['PR']),2),row['GA'],row['PA'], float(row['CoV_MFM']),float(row['CoV_INV']),row['PR_Alarm'],row['MFM_CoV_Alarm'],row['INV_CoV_Alarm'],row['Status']):
                            pass
                        connStr.commit()
                    except Exception as e:
                        pass
                cursor.close()
                connStr.close()
            except Exception as e:
                print(e)
                
    else:
        print('Sleeping!')
    time.sleep(3600)
